/*
 * Copyright © 2007-2022 Dynare Team
 *
 * This file is part of Dynare.
 *
 * Dynare is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Dynare is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Dynare.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef _ERROR_HANDLING_HH
#define _ERROR_HANDLING_HH

#include <string>
#include <sstream>
#include <cmath>

using namespace std;

struct GeneralException
{
  const string message;
};

struct FloatingPointException : public GeneralException
{
  FloatingPointException(const string &details) :
    GeneralException {"Floating point error: " + details}
  {
  }
};

struct LogException : public FloatingPointException
{
  LogException(double value) :
    FloatingPointException { [=]
    {
      // We don’t use std::to_string(), because it uses fixed formatting
      ostringstream s;
      s << "log(X) with X=" << defaultfloat << value;
      return s.str();
    }() }
  {
  }
};

struct Log10Exception : public FloatingPointException
{
  Log10Exception(double value) :
    FloatingPointException { [=]
    {
      // We don’t use std::to_string(), because it uses fixed formatting
      ostringstream s;
      s << "log10(X) with X=" << defaultfloat << value;
      return s.str();
    }() }
  {
  }
};

struct DivideException : public FloatingPointException
{
  DivideException(double divisor) :
    FloatingPointException { [=]
    {
      // We don’t use std::to_string(), because it uses fixed formatting
      ostringstream s;
      s << "a/X with X=" << defaultfloat << divisor;
      return s.str();
    }() }
  {
  }
};

struct PowException : public FloatingPointException
{
  PowException(double base, double exponent) :
    FloatingPointException { [=]
    {
      // We don’t use std::to_string(), because it uses fixed formatting
      ostringstream s;
      s << "X^a with X=" << defaultfloat << base;
      if (fabs(base) <= 1e-10)
        s << " and a=" << exponent;
      return s.str();
    }() }
  {
  }
};

struct UserException : public GeneralException
{
  UserException() : GeneralException {"User break"}
  {
  }
};

struct FatalException : public GeneralException
{
  FatalException(const string &details) :
    GeneralException {"Fatal error: " + details}
  {
  }
};

inline void
test_mxMalloc(void *z, int line, const string &file, const string &func, int amount)
{
  if (!z && amount > 0)
    throw FatalException{"mxMalloc: out of memory " + to_string(amount) + " bytes required at line " + to_string(line) + " in function " + func + " (file " + file};
}

#ifdef MATLAB_MEX_FILE
extern "C" bool utIsInterruptPending();
#endif

#endif // _ERROR_HANDLING_HH
